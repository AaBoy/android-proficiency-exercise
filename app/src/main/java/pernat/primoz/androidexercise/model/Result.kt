package pernat.primoz.androidexercise.model

data class Result(
        val title: String,
        val rows: List<Row>
)