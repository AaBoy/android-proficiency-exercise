package pernat.primoz.androidexercise.anime.shareElement

import android.view.View
import pernat.primoz.androidexercise.model.Row

interface AnimateFactsImage {
    fun onAnimateItemClick(fact: Row, view: View)
}