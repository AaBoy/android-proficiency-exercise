package pernat.primoz.androidexercise.ui.main.detailFacts

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import pernat.primoz.androidexercise.R
import pernat.primoz.androidexercise.databinding.ActivityDetailFactBinding
import pernat.primoz.androidexercise.model.Row

class DetailFactActivity : AppCompatActivity() {
    companion object {
        val EXTRA_FACT = "extra_fact_image"
        val EXTRA_FACT_IMAGE_TRANSITION_NAME = "extra_fact_image_transition_name"
    }

    lateinit var binding: ActivityDetailFactBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_fact)

        val extras = intent.extras
        val facts: Row = extras.getParcelable(EXTRA_FACT)

        binding.row = facts
        binding.avatarImage.transitionName = extras.getString(EXTRA_FACT_IMAGE_TRANSITION_NAME)
    }
}
