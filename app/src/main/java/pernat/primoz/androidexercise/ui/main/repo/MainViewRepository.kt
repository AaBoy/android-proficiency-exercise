package pernat.primoz.androidexercise.ui.main.repo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import pernat.primoz.androidexercise.model.Result
import pernat.primoz.androidexercise.network.ApiResponse
import pernat.primoz.androidexercise.network.RetroApi
import pernat.primoz.androidexercise.testing.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class MainViewRepository @Inject constructor(val api: RetroApi) {
    val compositeDisposable by lazy { CompositeDisposable() }

    var responseLiveData: MutableLiveData<ApiResponse<Result>> = MutableLiveData()

    fun getListData(): LiveData<ApiResponse<Result>> {

        compositeDisposable.add(api.getFacts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {
                    responseLiveData.postValue(ApiResponse(it, null))
                }
                .doOnError {
                    responseLiveData.postValue(ApiResponse(null, it))
                }
                .doFinally {
                    compositeDisposable.clear()
                }
                .subscribe())

        return responseLiveData
    }
}