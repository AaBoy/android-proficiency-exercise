package pernat.primoz.androidexercise.ui.main.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import pernat.primoz.androidexercise.R
import pernat.primoz.androidexercise.adapter.CanadaAdapterRecycler
import pernat.primoz.androidexercise.anime.shareElement.AnimateFactsImage
import pernat.primoz.androidexercise.databinding.ActivityMainBinding
import pernat.primoz.androidexercise.helper.BaseActivity
import pernat.primoz.androidexercise.model.Row
import pernat.primoz.androidexercise.ui.main.detailFacts.DetailFactActivity
import pernat.primoz.androidexercise.ui.main.viewmodel.MainViewModel
import javax.inject.Inject

class MainActivity : BaseActivity(), AnimateFactsImage {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var mainViewModel: MainViewModel

    lateinit var binding: ActivityMainBinding
    lateinit var canadaAdapterRecycler: CanadaAdapterRecycler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        setSupportActionBar(binding.toolbar)
        setToolbarTitle(getString(R.string.android_example))

        canadaAdapterRecycler = CanadaAdapterRecycler(this)
        binding.recyclerAdapter.layoutManager = LinearLayoutManager(this)
        binding.recyclerAdapter.adapter = canadaAdapterRecycler

        startSearch()
    }

    private fun setViewObservable() {
        mainViewModel.getFactsList().observe(this, Observer {
            when (it?.error) {
                null -> {
                    if (binding.swipeRefresh.isRefreshing) {
                        binding.swipeRefresh.isRefreshing = false
                    }

                    setToolbarTitle(it?.result?.title)

                    canadaAdapterRecycler.setFacts(it?.result!!)
                    binding.recyclerAdapter.visibility = View.VISIBLE
                }
                else -> Toast.makeText(this, getString(R.string.error_fetching_data), Toast.LENGTH_SHORT).show()
            }
        })

        binding.swipeRefresh.setOnRefreshListener {
            if (binding.swipeRefresh.isRefreshing) {
                startSearch()
            }
        }
    }

    private fun setToolbarTitle(string: String?) {
        supportActionBar?.title = string
    }

    private fun startSearch() {
        if (canadaAdapterRecycler.result != null) {
            canadaAdapterRecycler.result = null
            canadaAdapterRecycler.notifyDataSetChanged()
        }

        binding.swipeRefresh.isRefreshing = true

        setViewObservable()
    }

    override fun onAnimateItemClick(fact: Row, view: View) {
        val intent = Intent(this, DetailFactActivity::class.java)

        intent.putExtra(DetailFactActivity.EXTRA_FACT, fact)
        intent.putExtra(DetailFactActivity.EXTRA_FACT_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(view))

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                view,
                ViewCompat.getTransitionName(view))

        startActivity(intent, options.toBundle())
    }
}
