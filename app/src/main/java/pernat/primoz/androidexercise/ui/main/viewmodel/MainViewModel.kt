package pernat.primoz.androidexercise.ui.main.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import pernat.primoz.androidexercise.model.Result
import pernat.primoz.androidexercise.network.ApiResponse
import pernat.primoz.androidexercise.testing.OpenForTesting
import pernat.primoz.androidexercise.ui.main.repo.MainViewRepository
import javax.inject.Inject

@OpenForTesting
class MainViewModel @Inject constructor(val repository: MainViewRepository) : ViewModel() {
    private val apiListData = MediatorLiveData<ApiResponse<Result>>()

    fun getFactsList(): LiveData<ApiResponse<Result>> {
        apiListData.removeSource(repository.getListData())
        apiListData.addSource(repository.getListData(), Observer { apiResponse ->
            apiListData.postValue(apiResponse)
        })
        return apiListData
    }
}