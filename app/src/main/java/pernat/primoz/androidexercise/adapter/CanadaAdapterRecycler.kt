package pernat.primoz.androidexercise.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pernat.primoz.androidexercise.R
import pernat.primoz.androidexercise.adapter.view_holder_interactions.ItemFactClick
import pernat.primoz.androidexercise.anime.shareElement.AnimateFactsImage
import pernat.primoz.androidexercise.databinding.ItemViewBinding
import pernat.primoz.androidexercise.model.Result
import pernat.primoz.androidexercise.model.Row

class CanadaAdapterRecycler(val animateFactsImage: AnimateFactsImage) : RecyclerView.Adapter<CanadaItemViewHolder>(), ItemFactClick {

    var result: Result? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CanadaItemViewHolder {
        val binding: ItemViewBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), viewType, parent, false)
        return CanadaItemViewHolder(binding)
    }

    override fun getItemCount(): Int = result?.rows?.size ?: 0


    override fun onBindViewHolder(holder: CanadaItemViewHolder, position: Int) {
        holder.populateView(result?.rows!![position], this)
    }

    override fun getItemViewType(position: Int): Int = R.layout.item_view

    override fun onFactClick(fact: Row, view: View) {
        animateFactsImage.onAnimateItemClick(fact, view)
    }

    fun setFacts(newResult: Result) {
        result = null
        result = newResult
        notifyDataSetChanged()
    }
}

class CanadaItemViewHolder(val binding: ItemViewBinding) : RecyclerView.ViewHolder(binding.root) {

    fun populateView(row: Row, canadaAdapterRecycler: CanadaAdapterRecycler) {
        binding.row = row
        binding.factClick = canadaAdapterRecycler
        binding.avatarImage.transitionName = row.description ?: "" + row.title ?: ""

        binding.executePendingBindings()
    }
}