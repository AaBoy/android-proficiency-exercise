package pernat.primoz.androidexercise.adapter.view_holder_interactions

import android.view.View
import pernat.primoz.androidexercise.model.Row

interface ItemFactClick {
    fun onFactClick(fact: Row, view: View)
}