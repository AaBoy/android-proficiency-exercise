package pernat.primoz.androidexercise.network

import io.reactivex.Single
import pernat.primoz.androidexercise.model.Result
import retrofit2.http.GET

interface RetroApi {

    @GET("facts.json")
    fun getFacts(): Single<Result>
}