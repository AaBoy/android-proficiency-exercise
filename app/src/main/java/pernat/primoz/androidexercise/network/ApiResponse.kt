package pernat.primoz.androidexercise.network

class ApiResponse<T> constructor(val result: T?, val error: Throwable?)