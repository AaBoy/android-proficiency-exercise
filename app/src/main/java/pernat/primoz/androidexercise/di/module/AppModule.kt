package pernat.primoz.androidexercise.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import pernat.primoz.androidexercise.BuildConfig
import pernat.primoz.androidexercise.network.RetroApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient =
            OkHttpClient.Builder()
                    .build()


    @Provides
    @Singleton
    fun provideApiService(gson: Gson, okHttpClient: OkHttpClient): RetroApi =
            Retrofit.Builder()
                    .baseUrl(BuildConfig.SUBDOMAIN)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build().create(RetroApi::class.java)
}