package pernat.primoz.androidexercise.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import pernat.primoz.androidexercise.app.MyApplication
import pernat.primoz.androidexercise.di.DaggerActivity
import pernat.primoz.androidexercise.di.module.AppModule
import pernat.primoz.androidexercise.di.module.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class, ViewModelModule::class, AppModule::class,
        DaggerActivity::class))
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: MyApplication)
}