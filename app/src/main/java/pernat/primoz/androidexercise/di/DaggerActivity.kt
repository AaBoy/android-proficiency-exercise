package pernat.primoz.androidexercise.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pernat.primoz.androidexercise.di.module.MainActivityModule
import pernat.primoz.androidexercise.ui.main.main.MainActivity

@Module
abstract class DaggerActivity {
    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    abstract fun bindMainActivity(): MainActivity
}