package pernat.primoz.androidexercise.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pernat.primoz.androidexercise.di.viewmodelhelper.ViewModelFactory
import pernat.primoz.androidexercise.di.viewmodelhelper.ViewModelKey
import pernat.primoz.androidexercise.ui.main.viewmodel.MainViewModel

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindMainViewModel(manViewModel: MainViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}