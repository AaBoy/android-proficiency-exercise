package pernat.primoz.androidexercise.helper

import android.databinding.BindingAdapter
import android.widget.ImageView
import pernat.primoz.androidexercise.R


@BindingAdapter("app:loadImage")
fun goneUnless(imageView: ImageView, imgUrl: String?) {
    imageView.layout(0, 0, 0, 0)
    imageView.setImageDrawable(null)
    GlideApp.with(imageView.context)
            .load(imgUrl)
            .placeholder(R.drawable.canada_flag)
            .into(imageView)
}