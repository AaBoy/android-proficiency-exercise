package pernat.primoz.androidexercise.view

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import com.google.common.truth.Truth
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import pernat.primoz.androidexercise.model.Result
import pernat.primoz.androidexercise.model.Row
import pernat.primoz.androidexercise.network.ApiResponse
import pernat.primoz.androidexercise.ui.main.repo.MainViewRepository
import pernat.primoz.androidexercise.ui.main.viewmodel.MainViewModel
import pernat.primoz.androidexercise.utils.RxSameUnitSchedulerRule
import pernat.primoz.androidexercise.utils.testObserverValue

//https://medium.com/@nicolas.duponchel/testing-viewmodel-in-mvvm-using-livedata-and-rxjava-b27878495220
//https://github.com/NicolasDuponchel/RxBasics/blob/master/app/src/test/kotlin/duponchel/nicolas/rxbasics/MainViewModelTest.kt

class MainViewValues {
    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val rxSameSchedulerRule = RxSameUnitSchedulerRule()

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @InjectMocks
    private lateinit var mainView: MainViewModel


    private var repo: MainViewRepository = Mockito.mock(MainViewRepository::class.java)

    private val repoLiveData = MutableLiveData<ApiResponse<Result>>()

    @Before
    fun setup() {
        Mockito.`when`(repo.getListData()).thenReturn(repoLiveData)
    }

    @Test
    fun getList() {
        repoLiveData.postValue(ApiResponse(Result("title", arrayListOf(Row("title", "des", "im"))), null))

        val data = mainView.getFactsList().testObserverValue()

        Truth.assert_()
                .that(data.observedValues[0]?.result?.title)
                .isEqualTo(repoLiveData.value?.result?.title)

        Mockito.verify(repo, Mockito.atLeast(1)).getListData()
//        fail()
    }

    @Test
    fun getListError() {
        repoLiveData.postValue(ApiResponse(null, Throwable("error")))

        val data = mainView.getFactsList().testObserverValue()

        Truth.assert_()
                .that(data.observedValues[0]?.error)
                .isNotNull()

        Truth.assert_()
                .that(data.observedValues[0]?.result)
                .isNull()

        Mockito.verify(repo, Mockito.atLeast(1)).getListData()
    }
}