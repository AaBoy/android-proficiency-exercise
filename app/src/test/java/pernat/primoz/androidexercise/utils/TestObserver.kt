package pernat.primoz.androidexercise.utils

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer


class TestObserver<T> : Observer<T> {
    val observedValues = mutableListOf<T?>()

    override fun onChanged(t: T?) {
        observedValues.add(t)
    }
}

fun <T> LiveData<T>.testObserverValue() = TestObserver<T>().also {
    observeForever(it)
}