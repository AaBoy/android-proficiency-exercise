package pernat.primoz.androidexercise.ui.main

import android.arch.lifecycle.MutableLiveData
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import pernat.primoz.androidexercise.R
import pernat.primoz.androidexercise.app.MyApplication
import pernat.primoz.androidexercise.model.Result
import pernat.primoz.androidexercise.model.Row
import pernat.primoz.androidexercise.network.ApiResponse
import pernat.primoz.androidexercise.ui.main.main.MainActivity
import pernat.primoz.androidexercise.ui.main.viewmodel.MainViewModel
import pernat.primoz.androidexercise.util.ViewModelUtil
import pernat.primoz.androidexercise.util.createFakeActivityInjector


//https://proandroiddev.com/activity-espresso-test-with-daggers-android-injector-82f3ee564aa4

@RunWith(AndroidJUnit4::class)
class MainActivityView {
    @get:Rule
    val activityTestRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java, true, false) {
        override fun beforeActivityLaunched() {
            super.beforeActivityLaunched()
            val myApp = InstrumentationRegistry.getTargetContext().applicationContext as MyApplication
            myApp.activityDispatchingAndroidInjector = createFakeActivityInjector<MainActivity> {
                mainViewModel = mainView
                viewModelFactory = ViewModelUtil.createFor(mainView)
            }
        }
    }

    private var mainView = Mockito.mock(MainViewModel::class.java)

    private val repoLiveData = MutableLiveData<ApiResponse<Result>>()

    @Before
    fun setup() {
        Mockito.`when`(mainView.getFactsList()).thenReturn(repoLiveData)
    }

    @Test
    fun isListDisplayed() {
        repoLiveData.postValue(ApiResponse(Result("title", arrayListOf(Row("title", "des", "img"))), null))
        activityTestRule.launchActivity(null)
        onView(withId(R.id.recycler_adapter)).check(matches(isDisplayed()))
    }

    @Test
    fun isErrorShown() {
        repoLiveData.postValue(ApiResponse(null, Throwable("boom")))
        activityTestRule.launchActivity(null)
        onView(withText(R.string.error_fetching_data)).inRoot(withDecorView(not(`is`(activityTestRule.activity.window.decorView)))).check(matches(isDisplayed()))
    }
}