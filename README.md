 Android Proficiency Exercise
Overview
The purpose of this exercise is to assess candidate developer’s Android coding knowledge and style. The exercise involves building a “proof of concept” app which consumes a REST service and displays photos with headings and descriptions. The exercise will be evaluated on coding style, understanding of programming concepts, choice of techniques, and also by the developer’s process, as indicated by the trail of git commits.

Specification
Create an Android app which:
1. Ingests a json feed from https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json 􏰀
2. The feed contains a title and a list of rows 􏰀
3. The title in the toolbar should be updated from the json 􏰀
4. Displays the content (including image, title and description) as a list of rows 􏰀
5. Each row should be the right height to display its own content and no taller. No content should be clipped. 􏰀This means some rows will be larger than others. 􏰀
6. Loads the images lazily 􏰀
7. Don’t download them all at once, but only as needed 􏰀
8. Refresh function 􏰀-- either place a refresh button or use a pull-to-refresh
 9. Should not block UI when loading the data from the json feed. 􏰀

Guidelines
1. Use Git to manage the source code. A clear Git history showing your process is required. 􏰀
2. Scrolling the view should be smooth, even as images are downloading and getting added to the cells
3. Support both phone and tablets (in both orientations) 􏰀
4. Comment your code when necessary. 􏰀
5. Try to polish your code and the apps functionality as much as possible. 􏰀
6. Commit your changes to git in small chunks with meaningful comments
7. Choose your libraries and architecture as you would for an actual new project 􏰀starting from scratch (if you don’t have time to setup everything, explain your choices and what else you would do in an email once you submit your code)

Additional Requirements
1. Support all Android versions from API 21 (Lollipop)
